import axios from 'axios'

import {apiUrl} from '../config/config'

const WeatherServices = () => ({
	getWeather: (cityId, unit, apiKey) => {
		return axios
			.get(`${apiUrl}weather`, {
				params: {
					id: cityId,
					units: unit,
					appid: apiKey
				}
			})
			.then(response => response.data || {})
			.catch((error) => {
				console.log(error)
			})
	},
	getForecast: (coord, unit, apiKey) => {
		return axios
			.get(`${apiUrl}onecall`, {
				params: {
					...coord,
					exclude: 'current,minutely,hourly,alerts',
					units: unit,
					appid: apiKey
				}
			})
			.then(response => response.data || {})
			.catch((error) => {
				console.log(error)
			})
	},
	getHistory: (days = 1, coord, unit, apiKey) => {
		const promises = []
		const commonParams = {...coord, units: unit, appid: apiKey}

		for (let i = 1; i <= days; i++) {
			const dtPast = new Date().getTime() - i * 24 * 60 * 60 * 1000

			promises.push(axios
				.get(`${apiUrl}onecall/timemachine`, {
					params: {
						...commonParams,
						dt: Math.floor(dtPast / 1000)
					}
				})
				.then(response => response.data || {})
				.catch((error) => {
					console.log(error)
				})
			)
		}

		return Promise.allSettled(promises).then(values => values)
	}
})

export default WeatherServices()
