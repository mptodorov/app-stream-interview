import {ReactComponent as Search} from '../assets/icons/icon-search.svg'

import {ReactComponent as ArrowDown} from '../assets/icons/icon-arrow-down.svg'
import {ReactComponent as ArrowUp} from '../assets/icons/icon-arrow-up.svg'
import {ReactComponent as ArrowRight} from '../assets/icons/icon-arrow-right.svg'
import {ReactComponent as ArrowDecrease} from '../assets/icons/icon-decrease.svg'
import {ReactComponent as ArrowIncrease} from '../assets/icons/icon-increase.svg'

import {ReactComponent as Sun} from '../assets/icons/icon-sun.svg'
import {ReactComponent as CloudSun} from '../assets/icons/icon-cloud-sun.svg'
import {ReactComponent as Cloud} from '../assets/icons/icon-cloud.svg'
import {ReactComponent as Rain} from '../assets/icons/icon-rain.svg'

import {ReactComponent as Pressure} from '../assets/icons/icon-pressure.svg'
import {ReactComponent as Temperature} from '../assets/icons/icon-temperature.svg'
import {ReactComponent as Wind} from '../assets/icons/icon-wind.svg'

export const apiUrl = 'https://api.openweathermap.org/data/2.5/'

export const defaults = {
	apiKey: 'bb8a12cc87bfe9d17f974a5f36821c09',
	city: {
		id: 728193,
		name: 'Plovdiv'
	},
	unit: 'metric',
	units: {
		metric: {
			id: 'metric',
			sign: 'C',
			multiplier: 1
		},
		imperial: {
			id: 'imperial',
			sign: 'F',
			multiplier: 1
		}
	},
	background: 'main',
	backgrounds: {
		main: '#eea594',
		sunny: '#eeb625',
		cloudy: '#eeb625',
		rainy: '#61a9a6',
		snowy: '#61a9a6'
	},
	imagesPath: 'assets',
	images: {
		sunny: {
			name: 'weather-sun.png',
			alt: 'Sunny weather'
		},
		cloudy: {
			name: 'weather-sun.png',
			alt: 'Cloudy weather'
		},
		rainy: {
			name: 'weather-rain.png',
			alt: 'Rainy weather'
		},
		snowy: {
			name: 'weather-rain.png',
			alt: 'Snowy weather'
		}
	}
}

export const icons = {
	Search: {
		icon: Search,
		width: '12',
		height: '14'
	},
	ArrowDown: {
		icon: ArrowDown,
		width: '13',
		height: '8'
	},
	ArrowUp: {
		icon: ArrowUp,
		width: '13',
		height: '8'
	},
	ArrowRight: {
		icon: ArrowRight,
		width: '4',
		height: '7'
	},
	ArrowDecrease: {
		icon: ArrowDecrease,
		width: '7',
		height: '7'
	},
	ArrowIncrease: {
		icon: ArrowIncrease,
		width: '7',
		height: '7'
	},
	Sun: {
		icon: Sun,
		width: '29',
		height: '28'
	},
	CloudSun: {
		icon: CloudSun,
		width: '35',
		height: '20'
	},
	Cloud: {
		icon: Cloud,
		width: '24',
		height: '13'
	},
	Rain: {
		icon: Rain,
		width: '36',
		height: '35'
	},
	Pressure: {
		icon: Pressure,
		width: '21',
		height: '21'
	},
	Temperature: {
		icon: Temperature,
		width: '11',
		height: '29'
	},
	Wind: {
		icon: Wind,
		width: '28',
		height: '24'
	}
}
