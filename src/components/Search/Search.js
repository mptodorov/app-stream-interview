import React, {memo, useMemo, useState, useRef} from 'react'

import {deepEqual} from '../../utils/utils'

import IconButton from '../IconButton/IconButton'

import './Search.css'

const areEqual = (prevProps, nextProps) =>
	!(
		!deepEqual(prevProps['options'], nextProps['options']) ||
		!deepEqual(prevProps['defaultValue'], nextProps['defaultValue'])
	)

const Search = memo(({options = [], defaultValue = null, callback = null}) => {
	const [activeOption, setActiveOption] = useState({...defaultValue})
	const input = useRef(null)

	const optionsComponents = useMemo(() => options.map(({id, name = ''}) => (
		<p 
			title={name}
			key={'option-' + id}
			className={activeOption.id === id ? 'selected' : ''}
			onClick={() => {
				callback(id)
				setActiveOption({id, name})
			}}
		>
			{name}
		</p>
	)), [options, activeOption]) // eslint-disable-line
	const {name} = activeOption

	return (
		<div className='search'>
			<div className='search__fields'>
				<input 
					ref={input}
					type='text' 
					value={name}
					placeholder='' 
					className='search__field' 
					onChange={(event) => setActiveOption({...activeOption, name: event.target.value})}
				/>

				<div className='search__options'>
					{name
						? optionsComponents.filter(({props = {title: ''}}) => props.title.toLowerCase()
							.indexOf(name.toLowerCase()) === 0)
						: optionsComponents
					}
				</div>
			</div>

			<IconButton icon='ArrowDown' callback={() => input.current.focus()} />
		</div>
	)
}, areEqual)

export default Search
