import React from 'react'

import Icon from '../Icon/Icon'

import './IconButton.css'

const IconButton = ({text, callback, icon, width, height, ...rest}) => (
	<button className="btn-icon" onClick={callback} {...rest}>
		{text && (
			<span>
				{text}
			</span>
		)}

		<Icon icon={icon} width={width} height={height} />
	</button>
)

export default IconButton
