import React, {memo} from 'react'

import './Temperature.css'

const areEqual = (prevProps, nextProps) =>
	!(
		prevProps['actual'] !== nextProps['actual'] ||
		prevProps['feels'] !== nextProps['feels']
	)

const Temperature = memo(({actual = 0, feels = 0}) => (
	<div className="temperature">
		<strong>
			{actual}&deg;C
		</strong>

		<em>
			(feels like {feels}&deg;C)
		</em>
	</div>
), areEqual)

export default Temperature
