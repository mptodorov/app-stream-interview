import React, {memo} from 'react'

import {deepEqual} from '../../utils/utils'

import './Table.css'

const areEqual = (prevProps, nextProps) => deepEqual(prevProps['data'], nextProps['data'])

const Temperature = memo(({data = []}) => {
	const tableData = data.map(({title, value, unit}, index) => (
		<tr key={`table-row-${index}`}>
			<th>{title}</th>

			<td>{value}</td>

			<td>{unit}</td>
		</tr>
	))

	return (
		<table className="table">
			<tbody>
				{tableData}
			</tbody>
		</table>
	)
}, areEqual)

export default Temperature
