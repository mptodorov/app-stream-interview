import React from 'react'
import {connect} from 'react-redux'

import Icon from '../Icon/Icon'

import './Loader.css'

const Loader = (props) => {
	const {loader} = props

	return (
		<div className={'loader' + (loader.open ? ' shown' : '')}>
			<div className='loader__icon'>
				<Icon {...loader} />
			</div>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		loader: state.loader.fullLoader
	}
}

export default connect(
	mapStateToProps,
	null
)(Loader)