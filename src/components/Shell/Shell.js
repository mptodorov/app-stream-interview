import React from 'react'

import './Shell.css'

const Shell = ({children}) => (
	<div className='shell'>
		{children}
	</div>
)
export default Shell
