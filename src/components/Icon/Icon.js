import React from 'react'

import {icons} from '../../config/config'

const Icon = ({icon: iconName = 'Search', ...rest}) => {
	const icon = icons[iconName]
	const {width = icon.width, height = icon.height, ...attr} = rest
	const Icon = icon.icon

	return (
		<Icon width={width} height={height} {...attr} />
	)
}

export default Icon
