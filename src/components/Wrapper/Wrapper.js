import React from 'react'
import {connect} from 'react-redux'

import {getBackground} from '../../store/reducers/config'

import './Wrapper.css'

const Wrapper = ({background, children}) => (
	<div className='wrapper' style={{backgroundColor: background}}>
		{children}
	</div>
)

const mapStateToProps = (state) => ({
	background: getBackground(state)
})

export default connect(
	mapStateToProps
)(Wrapper)
