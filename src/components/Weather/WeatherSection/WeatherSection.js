import React from 'react'

import IconButton from '../../IconButton/IconButton'

import './WeatherSection.css'

const WeatherSection = (props) => {
	const {children, image = {}, width = 'auto', height = 'auto', actions = []} = props
	const {path, alt} = image

	return (
		<section className='section-weather'>
			{children}

			{path && (
				<img src={path} className='section__image' width={width} height={height} alt={alt} />
			)}

			{actions.length && (
				<div className="section__actions">
					{actions.map(({text, icon, callback}, index) => (
						<div key={`button-${index}`} className="section__actions-button">
							<IconButton text={text} icon={icon} callback={callback} />
						</div>
					))}
				</div>
			)}
		</section>
	)
}

export default WeatherSection
