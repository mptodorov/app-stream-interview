import React from 'react'
import {connect} from 'react-redux'

import {getPast, getFuture} from '../../store/reducers/weather'

import Table from '../Table/Table'

import WeatherArticle from './WeatherArticle/WeatherArticle'

const WeatherForecast = ({title, data = []}) => {
	const tableData = data.map(({date, temp}) => (
		{title: date.toLocaleDateString(), value: temp, unit: 'Celsius'}
	))

	return (
		<WeatherArticle>
			{title && (
				<h2>{title}</h2>
			)}

			<Table data={tableData} />
		</WeatherArticle>
	)
}

const mapStateToProps = (state, {past}) => ({
	data: past ? getPast(state) : getFuture(state)
})

export default connect(
	mapStateToProps
)(WeatherForecast)
