import React from 'react'

import './WeatherArticle.css'

const WeatherArticle = ({children}) => (
	<div className='weather-article'>
		{children}
	</div>
)

export default WeatherArticle
