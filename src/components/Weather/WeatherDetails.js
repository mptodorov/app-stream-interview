import React from 'react'
import {connect} from 'react-redux'

import {getCurrent} from '../../store/reducers/weather'

import Temperature from '../Temperature/Temperature'
import Table from '../Table/Table'

import WeatherArticle from './WeatherArticle/WeatherArticle'

const WeatherDetails = ({current}) => {
	const {weather, main, wind, clouds} = current
	const data = [
		{title: 'Pressure', value: main.pressure, unit: 'hPa'},
		{title: 'Humidity', value: main.humidity, unit: '%'},
		{title: 'Min temperature', value: main.temp_min, unit: 'Celsius'},
		{title: 'Max temperature', value: main.temp_max, unit: 'Celsius'},
		{title: 'Wind speed', value: wind.speed, unit: 'meter/sec'},
		{title: 'Wind direction', value: wind.deg, unit: 'Degree'},
		{title: 'Cloudiness', value: clouds.all, unit: '%'}
	]

	return (
		<WeatherArticle>
			<Temperature actual={main.temp} feels={main.feels_like} />

			<h2>
				{weather.main}
			</h2>

			<p>
				{weather.description}
			</p>

			<Table data={data} />
		</WeatherArticle>
	)
}

const mapStateToProps = (state) => ({
	current: getCurrent(state)
})

export default connect(
	mapStateToProps
)(WeatherDetails)
