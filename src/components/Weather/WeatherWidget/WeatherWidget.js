import React, {Component} from 'react'
import {connect} from 'react-redux'

import {loadForecast} from '../../../store/actions/weather'
import {showFullLoader} from '../../../store/actions/loader'
import {getCurrentState, getCurrentTemperature} from '../../../store/reducers/weather'
import {getCity, getApiKey, getUnit} from '../../../store/reducers/config'

import cities from '../../../config/city-list-bulgaria.json'

import Search from '../../Search/Search'
import Icon from '../../Icon/Icon'

import './WeatherWidget.css'

class WeatherWidget extends Component {
	loadForecast = (id) => {
		const {apiKey, unit, loadForecast, showFullLoader} = this.props

		if (id) {
			showFullLoader({icon: 'Sun', width: '70', height: '70'})
			loadForecast(id, unit.id, apiKey)
		} else {
			// Notification
			console.log('City is not included.')
		}
	}

	render() {
		const {weatherState, temperature, city, unit, fixed} = this.props
		let icon = 'Sun'

		switch (weatherState) {
			case 'Clear':
			case 'Clouds':
				icon = 'Sun'
				break
			default:
				icon = 'CloudSun'
				break
		}

		return (
			<div className={'weather-widget' + (fixed ? ' fixed' : '')}>
				<Search options={cities} defaultValue={city} callback={this.loadForecast} />

				{temperature && (
					<>
						<div className='weather-widget__temperature'>
							{Math.round(temperature)}

							<div className='weather-widget__details'>
								&deg;

								<small>{unit.sign}</small>

								<Icon icon={icon} className='weather-widget__icon' />
							</div>
						</div>

						<strong className='weather-widget__title'>
							{weatherState}
						</strong>
					</>
				)}
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	weatherState: getCurrentState(state),
	temperature: getCurrentTemperature(state),
	apiKey: getApiKey(state),
	city: getCity(state),
	unit: getUnit(state)
})

const mapDispatchToProps = (dispatch) => ({
	loadForecast: (cityId, unit, apiKey) => dispatch(loadForecast(cityId, unit, apiKey)),
	showFullLoader: (loaderProps) => dispatch(showFullLoader(loaderProps))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WeatherWidget)
