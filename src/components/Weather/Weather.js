import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'

import Wrapper from '../Wrapper/Wrapper'
import Shell from '../Shell/Shell'

import {getImageFromCondition} from '../../utils/utils'

import {loadForecast} from '../../store/actions/weather'
import {showFullLoader} from '../../store/actions/loader'
import {getCurrentState} from '../../store/reducers/weather'
import {getConfig, getBackground} from '../../store/reducers/config'

import WeatherSection from './WeatherSection/WeatherSection'
import WeatherWidget from './WeatherWidget/WeatherWidget'
import WeatherDetails from './WeatherDetails'
import WeatherForecast from './WeatherForecast'

import './Weather.css'

const Weather = (props) => {
	const [pageScroll, setPageScroll] = useState(0)
	const {background, weatherState} = props

	useEffect(() => {
		const {config, loadForecast, showFullLoader} = props
		const {city, unit, apiKey} = config

		showFullLoader({icon: 'Sun', width: '70', height: '70'})
		loadForecast(city.id, unit, apiKey)
	}, []) // eslint-disable-line

	return (
		<Wrapper background={background}>
			<Shell>
				<div className="weather" style={{transform: `translateY(-${pageScroll * 100}vh)`}}>
					<WeatherSection
						image={getImageFromCondition(weatherState)}
						actions={[
							{text: 'More details', icon: 'ArrowDown', callback: () => setPageScroll(1)}
						]}
					>
						<WeatherWidget />
					</WeatherSection>

					<WeatherSection
						actions={[
							{text: 'Home', icon: 'ArrowUp', callback: () => setPageScroll(0)},
							{text: 'Next/Prev 5 days', icon: 'ArrowDown', callback: () => setPageScroll(2)}
						]}
					>
						<WeatherDetails />
					</WeatherSection>

					<WeatherSection
						actions={[
							{text: 'Today\'s details', icon: 'ArrowUp', callback: () => setPageScroll(1)}
						]}
					>
						<WeatherForecast title="Next 5 days" />

						<WeatherForecast title="Past 5 days" past />
					</WeatherSection>
				</div>
			</Shell>
		</Wrapper>
	)
}

const mapStateToProps = (state) => ({
	background: getBackground(state),
	config: getConfig(state),
	weatherState: getCurrentState(state)
})

const mapDispatchToProps = (dispatch) => ({
	loadForecast: (cityId, unit, apiKey) => dispatch(loadForecast(cityId, unit, apiKey)),
	showFullLoader: (loaderProps) => dispatch(showFullLoader(loaderProps))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Weather)
