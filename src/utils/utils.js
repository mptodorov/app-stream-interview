import {defaults} from '../config/config'

const {imagesPath} = defaults
const {sunny, rainy} = defaults['images']

const isObject = (object) => object !== null && typeof object === 'object'

export const getImageFromCondition = (condition) => {
	let image = {}

	switch (condition) {
		case 'Clear':
		case 'Clouds':
			image = sunny
			break
		default:
			image = rainy
			break
	}

	return {
		path: `/${imagesPath}/${image.name}`,
		alt: image.alt
	}
}

export const getBackgroundFromCondition = (condition) => {
	switch (condition) {
		case 'Clear':
		case 'Clouds':
			return 'sunny'
		default:
			return 'rainy'
	}
}

export const deepEqual = (object1, object2) => {
	const keys1 = Object.keys(object1)
	const keys2 = Object.keys(object2)

	if (typeof object1 !== typeof object2 || keys1.length !== keys2.length) {
		return false
	}

	for (const key of keys1) {
		const val1 = object1[key]
		const val2 = object2[key]
		const areObjects = isObject(val1) && isObject(val2)

		if (
			(areObjects && !deepEqual(val1, val2)) ||
			(!areObjects && val1 !== val2)
		) {
			return false
		}
	}

	return true
}
