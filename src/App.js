import React from 'react'

import Weather from './components/Weather/Weather'
import Loader from './components/Loader/Loader'

const App = () => (
	<>
		<Weather />

		<Loader />
	</>
)

export default App