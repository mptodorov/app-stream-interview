import {all, call, put, takeLatest} from 'redux-saga/effects'

import {LOAD_FORECAST, loadForecastSuccess} from '../../actions/weather'
import {updateConfig} from '../../actions/config'
import {closeFullLoader} from '../../actions/loader'
import WeatherServices from '../../../services/WeatherServices'

import {getBackgroundFromCondition} from '../../../utils/utils'

function* fetchForecast({cityId, unit, apiKey}) {
	try {
		const weather = yield call(WeatherServices.getWeather, cityId, unit, apiKey)

		if (weather) {
			const {id, name, coord} = weather
			const background = yield call(getBackgroundFromCondition, weather.weather[0].main)
			
			// Get forecast
			const forecast = yield call(WeatherServices.getForecast, coord, unit, apiKey)
			const forecastTransformed = forecast.daily.splice(1, 5).map(({dt, temp}) => ({
				date: new Date(dt * 1000),
				temp: temp.day
			}))

			// Get history
			const history = yield call(WeatherServices.getHistory, 5, coord, unit, apiKey)
			const historyTransformed = history.reduce((acc, day) => {
				if (day.status && day.status === 'fulfilled' && day.value) {
					const {current} = day.value

					acc.push({date: new Date(current.dt * 1000), temp: current.temp})
				}

				return acc
			}, [])

			yield put(updateConfig({
				background,
				city: {id, name}
			}))
			yield put(loadForecastSuccess(weather, forecastTransformed, historyTransformed))
		} else {
			// Notification
		}
	} catch(error) {
		console.log(error)
	}

	yield put(closeFullLoader())
}

export function* watchLastFetchUser() {
	yield all([
		yield takeLatest(LOAD_FORECAST, fetchForecast)
	])
}
