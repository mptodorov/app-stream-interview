import {fork, call} from 'redux-saga/effects'

import {watchLastFetchUser} from './weather'

function* fetchAll() {
	yield fork(watchLastFetchUser)
}

function* rootSaga() {
	yield call(fetchAll)
}

export default rootSaga
