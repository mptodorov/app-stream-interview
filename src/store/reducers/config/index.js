import {defaults} from '../../../config/config'

import {UPDATE_CONFIG} from '../../actions/config'

const initialState = JSON.parse(JSON.stringify(defaults))

const updateConfig = (state, config) => ({
	...state,
	...config
})

const reducers = (state = initialState, {type, config}) => {
	switch (type) {
		case UPDATE_CONFIG:
			return updateConfig(state, config)
		default:
			return state
	}
}

export const getConfig = (state) => state.config
export const getApiKey = (state) => state.config.apiKey
export const getCity = (state) => state.config.city
export const getBackground = (state) => state.config.backgrounds[state.config.background]
export const getUnit = (state) => state.config.units[state.config.unit]

export default reducers
