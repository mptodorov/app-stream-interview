import {
	SHOW_FULL_LOADER,
	SHOW_LOCALE_LOADER,
	CLOSE_FULL_LOADER,
	CLOSE_LOCALE_LOADER
} from '../../actions/loader'

const initialState = {
	fullLoader: {},
	localeLoader: {}
}

const showFullLoader = (state, loaderProps) => ({
	...state,
	fullLoader: {
		...loaderProps,
		open: true
	}
})

const showLocaleLoader = (state, loaderProps) => ({
	...state,
	localeLoader: {
		...loaderProps,
		open: true
	}
})

const closeFullLoader = (state) => ({
	...state,
	fullLoader: {
		...state.fullLoader,
		open: false
	}
})

const closeLocaleLoader = (state) => ({
	...state,
	localeLoader: {
		...state.localeLoader,
		open: false
	}
})

const reducers = (state = initialState, {type, loaderProps}) => {
	switch (type) {
		case SHOW_FULL_LOADER:
			return showFullLoader(state, loaderProps)
		case SHOW_LOCALE_LOADER:
			return showLocaleLoader(state, loaderProps)
		case CLOSE_FULL_LOADER:
			return closeFullLoader(state)
		case CLOSE_LOCALE_LOADER:
			return closeLocaleLoader(state)
		default:
			return state
	}
}

export default reducers
