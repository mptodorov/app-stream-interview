import {LOAD_FORECAST_SUCCESS} from '../../actions/weather'

const initialState = {
	current: {
		weather: {},
		main: {},
		wind: {},
		clouds: {}
	},
	forecast: []
}

const loadForecastSuccess = (state, weather, forecast, history) => {
	const {weather: current, main, wind, clouds} = weather

	return {
		...state,
		current: {
			weather: current[0],
			main,
			wind,
			clouds
		},
		forecast,
		history
	}
}

const reducers = (state = initialState, {type, weather, forecast, history}) => {
	switch (type) {
		case LOAD_FORECAST_SUCCESS:
			return loadForecastSuccess(state, weather, forecast, history)
		default:
			return state
	}
}

export const getCurrent = (state) => state.weather.current
export const getCurrentState = (state) => state.weather.current.weather.main
export const getCurrentTemperature = (state) => state.weather.current.main.temp
export const getPast = (state) => state.weather.history
export const getFuture = (state) => state.weather.forecast

export default reducers
