import {combineReducers} from 'redux'

import config from './config'
import loader from './loader'
import weather from './weather'

export default combineReducers({
	config,
	loader,
	weather
})
