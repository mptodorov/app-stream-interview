export const UPDATE_CONFIG = 'UPDATE_CONFIG'

export const updateConfig = (config) => ({
	type: UPDATE_CONFIG,
	config
})
