export const SHOW_FULL_LOADER = 'SHOW_FULL_LOADER'
export const SHOW_LOCALE_LOADER = 'SHOW_LOCALE_LOADER'
export const CLOSE_FULL_LOADER = 'CLOSE_FULL_LOADER'
export const CLOSE_LOCALE_LOADER = 'CLOSE_LOCALE_LOADER'

export const showFullLoader = (loaderProps) => ({
	type: SHOW_FULL_LOADER,
	loaderProps
})

export const showLocaleLoader = (loaderProps) => ({
	type: SHOW_LOCALE_LOADER,
	loaderProps
})

export const closeFullLoader = () => ({
	type: CLOSE_FULL_LOADER
})

export const closeLocaleLoader = () => ({
	type: CLOSE_LOCALE_LOADER
})
