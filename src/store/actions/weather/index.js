export const LOAD_FORECAST = 'LOAD_FORECAST'
export const LOAD_FORECAST_SUCCESS = 'LOAD_FORECAST_SUCCESS'

export const loadForecast = (cityId, unit, apiKey) => ({
	type: LOAD_FORECAST,
	cityId,
	unit,
	apiKey
})

export const loadForecastSuccess = (weather, forecast, history) => ({
	type: LOAD_FORECAST_SUCCESS,
	weather,
	forecast,
	history
})
