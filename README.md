# Weather app for app-stream job interview

To start the project:

1. Checkout the project.
1. Enter the project's root folder.
1. Run in command window `npm install`.
1. Run in command window `npm start`.
